﻿using System;
using System.Runtime.InteropServices;

namespace UBHelper {
    /// <summary>
    /// Provides helper methods for the current player
    /// </summary>
    public static class Player {
        /// <summary>
        /// Sets a player option
        /// </summary>
        /// <param name="playerOption">PlayerOption to set</param>
        /// <param name="value">new value</param>
        public static void SetOption(PlayerOption playerOption, bool value) {
            if (GetOption(playerOption) != value) {
                P.Call_PlayerModule__SetOption((int)playerOption, value);
                P.Call_CPlayerModule__SaveToServer(true);
                // did this break something? 
                //if (saveToServer) SaveOptions();
            }
        }

        /// <summary>
        /// Gets a player option
        /// </summary>
        /// <param name="playerOption">PlayerOption to get</param>
        /// <returns>current value</returns>
        public static bool GetOption(PlayerOption playerOption) => P.Call_PlayerModule__GetOption((int)playerOption);

        /// <summary>
        /// Save the options to the server
        /// </summary>
        public static void SaveOptions() => P.Call_CPlayerModule__SaveToServer(true);

        /// <summary>
        /// Sets the players heading
        /// </summary>
        /// <param name="heading">Heading in degrees</param>
        unsafe public static void SetHeading(float heading) { P.Call_CPhysicsObj__set_heading(*P.CPhysicsPart__player_object, heading % 360.0f, 0); }

        unsafe public static void CPhysicsObj__SetHeading(float heading) => ((P.def_CPhysicsObj__set_heading)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00514C60, typeof(P.def_CPhysicsObj__set_heading)))(*P.CPhysicsPart__player_object, heading % 360.0f, 0);


        public unsafe static int? InqInt(int stype, int raw = 0, int allow_negative = 0) {
            int retval;
            if (P.Call_CBaseQualities__InqInt(P.CBaseQualities, stype, &retval, raw, allow_negative) == 1) { return retval; }
            return null;
        }
        public unsafe static long? InqInt64(int stype) {
            long retval;
            if (P.Call_CBaseQualities__InqInt64(P.CBaseQualities, stype, &retval) == 1) { return retval; }
            return null;
        }
        public unsafe static bool? InqBool(int stype) {
            bool retval;
            if (P.Call_CBaseQualities__InqBool(P.CBaseQualities, stype, &retval) == 1) { return retval; }
            return null;
        }
        public unsafe static double? InqFloat(int stype, int raw = 0) {
            double retval;
            if (P.Call_CBaseQualities__InqFloat(P.CBaseQualities, stype, &retval, raw) == 1) { return retval; }
            return null;
        }
        public unsafe static uint? InqDataID(int stype) {
            uint retval;
            if (P.Call_CBaseQualities__InqDataID(P.CBaseQualities, stype, &retval) == 1) { return retval; }
            return null;
        }
        public unsafe static uint? InqInstanceID(int stype) {
            uint retval;
            if (P.Call_CBaseQualities__InqInstanceID(P.CBaseQualities, stype, &retval) == 1) { return retval; }
            return null;
        }

        // Don't look at me like that. I see you looking.
        public unsafe static string InqString(int stype) {
            int ptr;
            try { ptr = *(int*)(P.CBaseQualities + 0x0018); } catch { return null; }
            if (ptr == 0) return null;
            int this_bucket = *(int*)((*(int*)(ptr + 0x08)) + ((stype % (*(int*)(ptr + 0x0C))) << 2));
            if (this_bucket == 0) { return null; }
            while (*(int*)this_bucket != stype) {
                this_bucket = *(int*)(this_bucket + 0x08);
                if (this_bucket == 0) { return null; }
            }
            if (this_bucket == 0 || *(int*)this_bucket != stype)
                return null;
            return Core.PStringBase_char_read((int*)(this_bucket + 0x04));
        }
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public enum PlayerOption {
            AutoRepeatAttack = 0x0,
            IgnoreAllegianceRequests = 0x1,
            IgnoreFellowshipRequests = 0x2,
            IgnoreTradeRequests = 0x3,
            DisableMostWeatherEffects = 0x4,
            PersistentAtDay = 0x5,
            AllowGive = 0x6,
            ViewCombatTarget = 0x7,
            ShowTooltips = 0x8,
            UseDeception = 0x9,
            ToggleRun = 0xA,
            StayInChatMode = 0xB,
            AdvancedCombatUI = 0xC,
            AutoTarget = 0xD,
            VividTargetingIndicator = 0xE,
            FellowshipShareXP = 0xF,
            AcceptLootPermits = 0x10,
            FellowshipShareLoot = 0x11,
            FellowshipAutoAcceptRequests = 0x12,
            SideBySideVitals = 0x13,
            CoordinatesOnRadar = 0x14,
            SpellDuration = 0x15,
            DisableHouseRestrictionEffects = 0x16,
            DragItemOnPlayerOpensSecureTrade = 0x17,
            DisplayAllegianceLogonNotifications = 0x18,
            UseChargeAttack = 0x19,
            UseCraftSuccessDialog = 0x1A,
            HearAllegianceChat = 0x1B,
            DisplayDateOfBirth = 0x1C,
            DisplayAge = 0x1D,
            DisplayChessRank = 0x1E,
            DisplayFishingSkill = 0x1F,
            DisplayNumberDeaths = 0x20,
            DisplayTimeStamps = 0x21,
            SalvageMultiple = 0x22,
            HearGeneralChat = 0x23,
            HearTradeChat = 0x24,
            HearLFGChat = 0x25,
            HearRoleplayChat = 0x26,
            AppearOffline = 0x27,
            DisplayNumberCharacterTitles = 0x28,
            MainPackPreferred = 0x29,
            LeadMissileTargets = 0x2A,
            UseFastMissiles = 0x2B,
            FilterLanguage = 0x2C,
            ConfirmVolatileRareUse = 0x2D,
            HearSocietyChat = 0x2E,
            ShowHelm = 0x2F,
            DisableDistanceFog = 0x30,
            UseMouseTurning = 0x31,
            ShowCloak = 0x32,
            LockUI = 0x33,
        };
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
}
