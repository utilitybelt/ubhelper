﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using Decal.Adapter;
using System.Text;

namespace UBHelper {
    /// <summary>
    /// Core UBHelper class
    /// </summary>
    public static unsafe class Core {
        /// <summary>
        /// UBHelper api version
        /// </summary>
        public static readonly int version = 2020112000;
        internal static string assemblyPath = "";
        internal static string pluginDirectory = "";
        internal static string CharacterName = "";
        /// <summary>
        /// Enables some lingering Debug messages. (caution- do not use in production, it makes a mess out of log files)
        /// </summary>
        public static bool Debug = false;

        /// <summary>
        /// Plugins should call this once at startup to intialize UBHelper
        /// </summary>
        /// <param name="characterName">name of the currently logged in character</param>
        public static void Startup(string characterName) {
            //WriteToDebugLog("UBHelper.Core.Startup()");
            Core.CharacterName = characterName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assemblyPath">absolute file path to the calling plugin assembly</param>
        /// <param name="storagePath">calling plugin file storage path</param>
        public static void FilterStartup(string assemblyPath, string storagePath) {
            Core.assemblyPath = System.IO.Path.GetDirectoryName(assemblyPath);
            pluginDirectory = storagePath;
            if (!System.IO.Path.GetFileName(assemblyPath).Equals("UtilityBelt.dll"))
                throw (new Exception("It is not safe to use this outside of UtilityBelt. Please contact the UtilityBelt development team for assistance. https://discord.gg/c75pPaz"));
            P.Write(P.ResolutionPatchWidth, 0); // minimum width
            P.Write(P.ResolutionPatchHeight, 0); // minimum height
            ConfirmationRequest.Enable();
            Vendor.Enable();
            P.CObjectMaint__UpdateVisibleObjectList.Setup(new P.def_CObjectMaint__UpdateVisibleObjectList(Hook_CObjectMaint__UpdateVisibleObjectList)); //RadarUpdate Event

            //GameState events
            P.CM_Character__Event_LoginCompleteNotification.Setup(new P.def_CM_Character__Event_LoginCompleteNotification(Hook_CM_Character__Event_LoginCompleteNotification));
            P.CM_UI__SendNotice_PlayerDescReceived.Setup(new P.def_CM_UI__SendNotice_PlayerDescReceived(Hook_CM_UI__SendNotice_PlayerDescReceived));
            P.Proto_UI__LogOffCharacter.Setup(new P.def_Proto_UI__LogOffCharacter(Hook_Proto_UI__LogOffCharacter));
            P.UIFlow__UseNewMode.Setup(new P.def_UIFlow__UseNewMode(Hook_UIFlow__UseNewMode));
            GameState = UBHelper.GameState.Game_Started;
        }
        public static void FilterShutdown() {
            ConfirmationRequest.Disable();
            Vendor.Disable();
            P.Cleanup();
            //WriteToDebugLog("UBHelper.Core.FilterShutdown()");
        }


        /// <summary>
        /// Plugins should call this once on shutdown to clean up
        /// </summary>
        public static void Shutdown() {
            Jumper.JumpCancel();
            VideoPatch.Enabled = false;
            //WriteToDebugLog("UBHelper.Core.Shutdown()");

        }

        /// <summary>
        /// Generic callbackback delegate with no arguments
        /// </summary>
        public delegate void callback();

        #region RadarUpdate (event)
        private static double lastStuckTime = 0d;
        internal static void Hook_CObjectMaint__UpdateVisibleObjectList(int CObjectMaint) {
            P.Call_CObjectMaint__UpdateVisibleObjectList(CObjectMaint);
            if (GameState == GameState.In_Game) {
                try {
                    RadarUpdate?.Invoke(*P.uptime);
                    if ((*P.ACCWeenieObject__prevRequest != 0) && (lastStuckTime != *P.ACCWeenieObject__prevRequestTime) && (*P.Timer__cur_time - *P.ACCWeenieObject__prevRequestTime) > 30d) {
                        Kevorkian?.Invoke(*P.ACCWeenieObject__prevRequest, *P.ACCWeenieObject__prevRequestTime, *P.Timer__cur_time, *P.ACCWeenieObject__prevRequestObjectID);
                        lastStuckTime = *P.ACCWeenieObject__prevRequestTime;
                        //Core.WriteToChat($"cur_time - prevRequestTime was > 30: *P.ACCWeenieObject__prevRequest={*P.ACCWeenieObject__prevRequest}, *P.ACCWeenieObject__prevRequestObjectID={*P.ACCWeenieObject__prevRequestObjectID}");
                    }
                } catch (Exception e) { Core.WriteToChat($"Error: EventHandler for UBHelper.Core.RadarUpdate threw an error: {e}", 22); }
            }
        }

        /// <summary>
        /// AC client uptime in seconds
        /// </summary>
        public static double Uptime { get => *P.uptime; }

        /// <summary>
        /// Fired roughly once a second when the radar system updates. Useful for things that don't
        /// need to be checked on every frame.
        /// </summary>
        public static event Del_RadarUpdate RadarUpdate;

        /// <summary>
        /// RadarUpdate event handler delegate
        /// </summary>
        /// <param name="uptime">AC client uptime in seconds</param>
        public delegate void Del_RadarUpdate(double uptime);

        /// <summary>
        /// Kevorkian Event
        /// </summary>
        public static event Del_Kevorkian Kevorkian;

        /// <summary>
        /// The esteemed Dr. Kevorkian
        /// </summary>
        public delegate void Del_Kevorkian(int prevRequest, double prevRequestTime, double curTime, int objectID);

        /// <summary>
        /// Returns true if the client currently has focus.
        /// </summary>
        public static bool isFocused => (*P.Device__m_bIsActiveApp == 1);

        /// <summary>
        /// Returns true if the mouse is currently over the client (regardless of focus)
        /// </summary>
        public static bool isTracked => (*P.Device__m_bTrackLeaveCalled == 1);
        public static int Client_HWND => *P.Device__m_hWnd;
        public static int VendorID { get { try { return *(int*)(*P.ClientUISystem + 0x20); } catch { return 0; } } }
        public static double curTime => *P.Timer__cur_time;
        public static int CPhysics => *P.CPhysicsPart__player_object;
        internal static int CommandInterpreter { get { try { return (*(int*)(*P.Smartbox + 0xB8)); } catch { return 0; } } }
        internal static int MovementManager { get { try { return *(int*)(CPhysics + 0x00C4); } catch { return 0; } } }
        internal static int CMotionInterp { get { try { return *(int*)(MovementManager + 0x0000); } catch { return 0; } } }
        internal static int ClientNet { get { try { return (*(int*)(*P.gmClient + 0x0100)); } catch { return 0; } } }
        internal static int ReceiverData { get { try { return *(int*)(ClientNet + 0x8820); } catch { return 0; } } }
        public static int UIPersistantData { get { try { return *(int*)(*(int*)(*P.gmClient + 0x00B0) + 0x0098); } catch { return 0; } } }
        #endregion
        #region GameStateChanged (event)
        internal static bool Hook_CM_Character__Event_LoginCompleteNotification() {
            GameState = UBHelper.GameState.In_Game;
            return P.Call_CM_Character__Event_LoginCompleteNotification();
        }
        internal static char Hook_CM_UI__SendNotice_PlayerDescReceived(int CACQualities, int CPlayerModule) {
            GameState = UBHelper.GameState.PlayerDesc_Received;
            return P.Call_CM_UI__SendNotice_PlayerDescReceived(CACQualities, CPlayerModule);
        }
        internal static bool Hook_Proto_UI__LogOffCharacter(int gid) {
            GameState = UBHelper.GameState.Logging_Out;
            return P.Call_Proto_UI__LogOffCharacter(gid);
        }
        internal static void Hook_UIFlow__UseNewMode(int UIFlow) {

            UIMode oldMode = *(UIMode*)(UIFlow + 0x8C);
            UIMode newMode = *(UIMode*)(UIFlow + 0x90);
            //WriteToDebugLog($"UIFlow__UseNewMode {oldMode} => {newMode}");
            P.Call_UIFlow__UseNewMode(UIFlow);
            switch (newMode) {
                case UIMode.CharacterManagementUI:
                    GameState = GameState.Character_Select_Screen;
                    break;
                case UIMode.CharGenMainUI:
                    GameState = GameState.Creating_Character;
                    break;
                case UIMode.DisconnectedUI:
                    GameState = GameState.Disconnected;
                    break;
                case UIMode.GamePlayUI:
                    GameState = GameState.Entering_Game;
                    break;
            }

        }

        /// <summary>
        /// Fired when UBHelper.Core.GameState changes.
        /// </summary>
        public static event Del_GameState GameStateChanged;

        /// <summary>
        /// Gamestate event handler delegate
        /// </summary>
        /// <param name="previous">Previous UBHelper.GameState</param>
        /// <param name="new_state">New UBHelper.GameState</param>
        public delegate void Del_GameState(GameState previous, GameState new_state);

        internal static GameState gameState = UBHelper.GameState.Initial;
        public static GameState GameState {
            get => gameState;
            internal set {
                if (value != gameState) {
                    GameState oldState = gameState;
                    gameState = value;
                    try {
                        GameStateChanged?.Invoke(oldState, gameState);
                    } catch { Core.WriteToDebugLog("Error: EventHandler for UBHelper.Core.GameStateChanged threw an error."); }
                    //WriteToDebugLog($"GameState {oldState} => {gameState}");
                }
            }
        }
        #endregion

        #region Busy Count / Busy State
        internal static int GetBusyCount => (*(int*)((*P.ClientUISystem) + 20));
        internal static void IncrementBusyCount() {
            P.Call_ClientUISystem_IncrementBusyCount();

        }
        internal static void DecrementBusyCount() {
            P.Call_ClientUISystem_DecrementBusyCount();
        }

        /// <summary>
        /// Clears the client busy count
        /// </summary>
        public static void ClearBusyCount() {
            *(int*)((*P.ClientUISystem) + 0x14) = 1;
            DecrementBusyCount();
        }

        /// <summary>
        /// Gets the clients current busy state
        /// </summary>
        /// <returns></returns>
        public static int GetBusyState => *P.ACCWeenieObject__prevRequest;

        /// <summary>
        /// Gets the clients current busy state id
        /// </summary>
        /// <returns></returns>
        public static int GetBusyStateId => *P.ACCWeenieObject__prevRequestObjectID;

        /// <summary>
        /// Gets the clients current busy state time
        /// </summary>
        /// <returns></returns>
        public static double GetBusyStateTime => *P.ACCWeenieObject__prevRequestTime;

        /// <summary>
        /// Clears the client busy state. This will also perform a *teapot* emote.
        /// </summary>
        /// <param name="force"></param>
        public static void ClearBusyState(bool force = true) {
            if (!force && ((*P.ACCWeenieObject__prevRequest == 0) || (*P.Timer__cur_time - *P.ACCWeenieObject__prevRequestTime) < 5d))
                return; // quietly return if busyState is already 0, or request time was less than 5 seconds ago
            *P.ACCWeenieObject__prevRequest = 0;
            *P.ACCWeenieObject__prevRequestObjectID = 0;
            *P.ACCWeenieObject__prevRequestTime = 0.0d;
            Decal.Adapter.CoreManager.Current.Actions.SetCombatMode(Decal.Adapter.Wrappers.CombatState.Peace);
            if (force)
                Decal.Adapter.CoreManager.Current.Actions.InvokeChatParser("*teapot*");
        }
        #endregion
        #region SendTellByGUID
        /// <summary>
        /// Send a tell to an objectGUID.  This is helpful for automating spell professors at least.
        /// </summary>
        /// <param name="targetId">object id to send a tell to</param>
        /// <param name="message">message to send</param>
        public static void SendTellByGUID(int targetId, string message) {
            int* ps_message = PStringBase_char_new(message);
            P.Call_CM_Communication__Event_TalkDirect((int)&ps_message, targetId);
            DisposeObject(ps_message);
        }
        #endregion

        #region Logging
        internal static void WriteToChat(string message, int color = 5) {
            try {
                string msg = "[UB] " + message;
                Decal.Adapter.CoreManager.Current?.Actions?.AddChatText(msg, color);
                vTank.Tell(msg, color, 0);
                WriteToDebugLog(message);
            } catch (Exception ex) { LogException(ex); }
        }

        /// <summary>
        /// Log an exception
        /// </summary>
        /// <param name="ex">exception to log</param>
        public static void LogException(Exception ex) {
            try {
                using (StreamWriter writer = new StreamWriter(Path.Combine(assemblyPath, "exceptions.txt"), true)) {

                    writer.WriteLine("============================================================================");
                    writer.WriteLine("UBHelper: " + ex.ToString());
                    writer.WriteLine("============================================================================");
                    writer.WriteLine(DateTime.Now.ToString());
                    writer.WriteLine("Error: " + ex.Message);
                    writer.WriteLine("Source: " + ex.Source);
                    writer.WriteLine("Stack: " + ex.StackTrace);
                    if (ex.InnerException != null) {
                        writer.WriteLine("Inner: " + ex.InnerException.Message);
                        writer.WriteLine("Inner Stack: " + ex.InnerException.StackTrace);
                    }
                    writer.WriteLine("============================================================================");
                    writer.WriteLine("");
                    writer.Close();
                }
            } catch { }
        }

        /// <summary>
        /// Writes a string to the characters debug log
        /// </summary>
        /// <param name="message">debug message to write</param>
        public static void WriteToDebugLog(string message) {
            File.AppendAllText(Path.Combine(assemblyPath, "ubhelper-log.txt"), $"{DateTime.Now.ToString("yy/MM/dd H:mm:ss")} {message}" + Environment.NewLine);
        }
        internal static void Error(string message) {
            WriteToChat("Error: " + message, 15);
        }
        #endregion
        #region TurnToHeading(float head)
        /// <summary>
        /// Turns your character to a specific heading in degrees. 0 is North, 90 is East.
        /// </summary>
        /// <param name="head">the heading to face your character towards, in degrees. 0 is North, 90 is East.</param>
        public static void TurnToHeading(float head) {
            P.Call_CommandInterpreter__TurnToHeading(head, 1);
        }
        #endregion
        #region SetResolution(ushort width, ushort height)
        /// <summary>
        /// Sets the game clients resolution to a custom window size
        /// </summary>
        /// <param name="width">new client width in pixels</param>
        /// <param name="height">new client height in pixels</param>
        public static void SetResolution(ushort width, ushort height) {
            *P.Device__m_DisplayPrefs_Resolution_Height = height;
            *P.Device__m_DisplayPrefs_Resolution_Width = width;
        }
        #endregion
        #region SetTextures(uint landscape, byte landscapeDetail, uint environment, byte environmentDetail, uint sceneryDraw, uint landscapeDraw)
        /// <summary>
        /// TODO: Sets texture details?
        /// </summary>
        /// <param name="landscape"></param>
        /// <param name="landscapeDetail"></param>
        /// <param name="environment"></param>
        /// <param name="environmentDetail"></param>
        /// <param name="sceneryDraw"></param>
        /// <param name="landscapeDraw"></param>
        public static void SetTextures(uint landscape, byte landscapeDetail, uint environment, byte environmentDetail, uint sceneryDraw, uint landscapeDraw) {
            *P.Device__m_DisplayPrefs_Landscape = landscape;
            *P.Device__m_DisplayPrefs_LandscapeDetail = landscapeDetail;
            *P.Device__m_DisplayPrefs_Environment = environment;
            *P.Device__m_DisplayPrefs_EnvironmentDetail = environmentDetail;
            *P.Device__m_DisplayPrefs_SceneryDraw = sceneryDraw;
            *P.Device__m_DisplayPrefs_LandscapeDraw = landscapeDraw;
        }
        #endregion
        #region MoveElement(UIElement element, System.Drawing.Rectangle pos)
        public static void MoveElement(UIElement element, System.Drawing.Rectangle pos) {
            int m_pGameplayUI = *(int*)(P.ResolveHandler(0x004DD284) + 4);
            int UIElement = P.Call_UIElement__GetChildRecursive(m_pGameplayUI, (int)element);
            P.Call_UIElement__MoveTo(UIElement, pos.X, pos.Y);
            P.Call_UIElement__ResizeTo(UIElement, pos.Width, pos.Height);
        }

        public static System.Drawing.Rectangle GetElementPosition(UIElement element) {
            int m_pGameplayUI = *(int*)(P.ResolveHandler(0x004DD284) + 4);
            int UIElement = P.Call_UIElement__GetChildRecursive(m_pGameplayUI, (int)element);
            return new System.Drawing.Rectangle(P.Call_UIRegion__GetX(UIElement), P.Call_UIRegion__GetY(UIElement), P.Call_UIRegion__GetWidth(UIElement), P.Call_UIRegion__GetHeight(UIElement));
        }
        #endregion
        #region PStringBase_char_new(string contents)

        internal static int* PStringBase_char_new(string contents) {
            void* s_NullBuffer = (void*)null;
            P.Call_AC1Legacy__PStringBase_char__PStringBase_char((int)&s_NullBuffer, contents);
            return (int*)s_NullBuffer;
        }
        internal static void DisposeObject(int* obj) {
            if (System.Threading.Interlocked.Decrement(ref *(int*)((int)obj + 4)) == 0)
                ((P.__vecDelDtor)Marshal.GetDelegateForFunctionPointer((IntPtr)(*(int*)*obj), typeof(P.__vecDelDtor)))((int)obj, 1);
        }
        #endregion
        #region PStringBase_char_read(int* pointer)
        internal static string PStringBase_char_read(int* pointer) {
            try {
                return Marshal.PtrToStringAnsi((IntPtr)(*pointer + 0x14), (*(int*)(*pointer + 0x08)) - 1);
            } catch { return ""; }
        }
        #endregion
        #region PSRefBufferCharData_char_read(int* pointer)
        internal static string PSRefBufferCharData_char_read(int* pointer) {
            try {
                return Marshal.PtrToStringAnsi((IntPtr)(*pointer));
            } catch { return ""; }
        }
        #endregion
        #region PSRefBufferCharData_ushort_read(int* pointer)
        internal static string PSRefBufferCharData_ushort_read(int* pointer) {
            try {
                return Marshal.PtrToStringUni((IntPtr)(*pointer));
            } catch { return ""; }
        }
        #endregion
        internal static int GetPhysicsPtr(int character_id) { try { return P.Call_CObjectMaint__GetObjectA(character_id); } catch { return 0; } }
        internal static int GetWeeniePtr(int character_id) { try { return P.Call_CObjectMaint__GetWeenieObject(character_id); } catch { return 0; } }

        public static int prevRequestObjectID => *P.ACCWeenieObject__prevRequestObjectID;
        public static int selectedID => *P.ACCWeenieObject__selectedID;
        public static int playerID => *P.CPhysicsPart__player_iid;

        /// <summary>
        /// Get Distance to a player, from the onscreen physics (dirty)
        /// </summary>
        /// <param name="character_id">character id</param>
        public static float DirtyDistance(int character_id) {
            float distance = (float)99999.0;
            try { distance = *(float*)(GetPhysicsPtr(character_id) + 0x20); } catch { }
            return distance;
        }
        #region Misc Strings

        /// <summary>
        /// Returns the Item ID of the currently open ground container (or 0)
        /// </summary>
        public static int gmExternalContainerUI_id { get { try { return *(int*)(P.gmExternalContainerUI + 0x0610); } catch { return 0; } } }

        /// <summary>
        /// Returns the scroll position of the External Container UI
        /// </summary>
        public static System.Drawing.Point gmExternalContainerUI_itemList_Position {
            get {
                try {
                    int boxy = P.gmExternalContainerUI_m_itemList;
                    return new System.Drawing.Point() { X = *(int*)(boxy + 0x05F8), Y = *(int*)(boxy + 0x05FC) };
                } catch { return new System.Drawing.Point(); }
            }
        }

        /// <summary>
        /// Returns the virtual size of the External Container UI (each item is 32x32 pixels)
        /// </summary>
        public static System.Drawing.Point gmExternalContainerUI_itemList_Size {
            get {
                try {
                    int boxy = P.gmExternalContainerUI_m_itemList;
                    return new System.Drawing.Point() { X = *(int*)(boxy + 0x0600), Y = *(int*)(boxy + 0x0604) };
                } catch { return new System.Drawing.Point(); }
            }
        }

        /// <summary>
        /// Returns the on-screen position of the External Container UI
        /// </summary>
        public static Decal.Interop.Core.tagRECT gmExternalContainerUI_position {
            get {
                try {
                    int boxy = *(int*)(P.gmExternalContainerUI + 0x00AC); // get pointer to UIElement *m_parent (floaty frame), and return the x, y, x, y of that
                    return new Decal.Interop.Core.tagRECT() { left = *(int*)(boxy + 0x7C), top = *(int*)(boxy + 0x80), right = *(int*)(boxy + 0x84), bottom = *(int*)(boxy + 0x88) };
                } catch { return new Decal.Interop.Core.tagRECT(); }
            }
        }

        /// <summary>
        /// Returns the ID of the Character Selected on the Character Select Screen, or the ID of a newly created character.
        /// </summary>
        public static int LoginCharacterID { get { try { return (*(int*)(UIPersistantData + 0x004C)); } catch { return 0; } } }

        /// <summary>
        /// Returns true if the player is currently in portal space
        /// </summary>
        public static bool TeleportInProgress { get { try { return (*(byte*)(*P.CPlayerModule + 0x0238) == (byte)1); } catch { return false; } } }

        /// <summary>
        /// Poll the player's username, useful for account-wide kinds of things.
        /// </summary>
        public static string UserName { get { try { return PStringBase_char_read((int*)(*P.gmClient + 0x0074)); } catch { return ""; } } }

        /// <summary>
        /// Poll the Server-reported WorldName
        /// </summary>
        public static string WorldName { get { try { return PSRefBufferCharData_ushort_read((int*)(*P.gmClient + 0x00A8)); } catch { return ""; } } }

        /// <summary>
        /// Poll the Server Address, used to initally connect to the server
        /// </summary>
        public static string ServerAddress { get { try { return PSRefBufferCharData_char_read((int*)(*P.gmClient + 0x008C)); } catch { return ""; } } }

        /// <summary>
        /// This should remain commented out in production releases. Used internally for account generation
        /// </summary>
        // internal static string Password { get { try { return PSRefBufferCharData_char_read((int*)(*P.gmClient + 0x0158)); } catch { return ""; } } }

        /// <summary>
        /// Return a Dictionary containing the most recent CharacterSet received from the server.
        /// </summary>
        public static Dictionary<int, string> CharacterSet {
            get {
                Dictionary<int, string> ret = new Dictionary<int, string>();
                try {
                    for (int i = 0; i < (*(int*)(UIPersistantData + 0x18)); i++)
                        ret.Add(*(int*)(*(int*)(UIPersistantData + 0x10) + 4 + (i << 4)), PStringBase_char_read((int*)(*(int*)(UIPersistantData + 0x10) + 8 + (i << 4))));
                } catch { }
                return ret;
            }
        }



        #endregion

        public static string HexDump(byte[] bytes, int bytesPerLine = 48) {
            if (bytes == null) return "<null>";
            int bytesLength = bytes.Length;

            char[] HexChars = "0123456789ABCDEF".ToCharArray();

            int firstHexColumn =
                  8                   // 8 characters for the address
                + 3;                  // 3 spaces

            int firstCharColumn = firstHexColumn
                + bytesPerLine * 3       // - 2 digit for the hexadecimal value and 1 space
                + (bytesPerLine - 1) / 8 // - 1 extra space every 8 characters from the 9th
                + 2;                  // 2 spaces 

            int lineLength = firstCharColumn
                + bytesPerLine           // - characters to show the ascii value
                + Environment.NewLine.Length; // Carriage return and line feed (should normally be 2)

            char[] line = (new String(' ', lineLength - Environment.NewLine.Length) + Environment.NewLine).ToCharArray();
            int expectedLines = (bytesLength + bytesPerLine - 1) / bytesPerLine;
            StringBuilder result = new StringBuilder(expectedLines * lineLength);

            for (int i = 0; i < bytesLength; i += bytesPerLine) {
                line[0] = HexChars[(i >> 28) & 0xF];
                line[1] = HexChars[(i >> 24) & 0xF];
                line[2] = HexChars[(i >> 20) & 0xF];
                line[3] = HexChars[(i >> 16) & 0xF];
                line[4] = HexChars[(i >> 12) & 0xF];
                line[5] = HexChars[(i >> 8) & 0xF];
                line[6] = HexChars[(i >> 4) & 0xF];
                line[7] = HexChars[(i >> 0) & 0xF];

                int hexColumn = firstHexColumn;
                int charColumn = firstCharColumn;

                for (int j = 0; j < bytesPerLine; j++) {
                    if (j > 0 && (j & 7) == 0) hexColumn++;
                    if (i + j >= bytesLength) {
                        line[hexColumn] = ' ';
                        line[hexColumn + 1] = ' ';
                        line[charColumn] = ' ';
                    } else {
                        byte b = bytes[i + j];
                        line[hexColumn] = HexChars[(b >> 4) & 0xF];
                        line[hexColumn + 1] = HexChars[b & 0xF];
                        line[charColumn] = (b < 32 ? '·' : (char)b);
                    }
                    hexColumn += 3;
                    charColumn++;
                }
                result.Append(line);
            }
            return result.ToString();
        }
    }

    public enum GameState {
        Initial = 0,
        Game_Started = 1,
        Character_Select_Screen = 2,
        Creating_Character = 3,
        Entering_Game = 4,
        PlayerDesc_Received = 5,
        In_Game = 6,
        Logging_Out = 7,
        Disconnected = 8
    }
    public enum UIMode {
        None = 0,
        IntroUI = 0x10000001,
        DisconnectedUI = 0x10000002,
        DataPatchUI = 0x10000003,
        CreditsUI = 0x10000005,
        GamePlayUI = 0x10000008,
        EpilogueUI = 0x10000009,
        CharacterManagementUI = 0x1000000A,
        CharGenMainUI = 0x1000000B
    }
}
