﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace UBHelper {
    /// <summary>
    /// Adds functionality to capture packets ingame, and save to pcap format.
    /// </summary>
    public static unsafe class PCap {
        /// <summary>
        /// Enable capturing packet data to a rolling buffer
        /// </summary>
        /// <param name="bufferDepth">the amount of packets to store in teh buffer</param>
        public static void Enable(int bufferDepth = 1000) {
            data = new Data(bufferDepth);
            if (P.Entrypoint_extern_sendto == 0) {
                P.Entrypoint_extern_sendto = *P.Ptr_extern_sendto;
                P.Del_extern_sendto = new P.extern_sendto(Hook_extern_sendto);
                P.Rtrn_extern_sendto = (P.extern_sendto)Marshal.GetDelegateForFunctionPointer((IntPtr)P.Entrypoint_extern_sendto, typeof(P.extern_sendto));
                P.Write((IntPtr)P.Ptr_extern_sendto, (int)Marshal.GetFunctionPointerForDelegate(P.Del_extern_sendto));
            }
            if (P.Entrypoint_extern_recvfrom == 0) {
                P.Entrypoint_extern_recvfrom = *P.Ptr_extern_recvfrom;
                P.Del_extern_recvfrom = new P.extern_recvfrom(Hook_extern_recvfrom);
                P.Rtrn_extern_recvfrom = (P.extern_recvfrom)Marshal.GetDelegateForFunctionPointer((IntPtr)P.Entrypoint_extern_recvfrom, typeof(P.extern_recvfrom));
                P.Write((IntPtr)P.Ptr_extern_recvfrom, (int)Marshal.GetFunctionPointerForDelegate(P.Del_extern_recvfrom));
            }
        }

        /// <summary>
        /// Disable packet capturing
        /// </summary>
        public static void Disable() {
            if (P.Entrypoint_extern_sendto != 0) {
                P.Write((IntPtr)P.Ptr_extern_sendto, P.Entrypoint_extern_sendto);
                P.Del_extern_sendto = null;
                P.Entrypoint_extern_sendto = 0;
            }
            if (P.Entrypoint_extern_recvfrom != 0) {
                P.Write((IntPtr)P.Ptr_extern_recvfrom, P.Entrypoint_extern_recvfrom);
                P.Del_extern_recvfrom = null;
                P.Entrypoint_extern_recvfrom = 0;
            }
            data = null;
        }

        internal static int Hook_extern_sendto(int s, int buf, int len, int flags, int to, int tolen) {
            int bytesSent = 0;
            try {
                bytesSent = P.Rtrn_extern_sendto(s, buf, len, flags, to, tolen);
            } catch { }
            if (len > 0)
                data.StorePacket(1, buf, len, to);
            return bytesSent;
        }

        internal static int Hook_extern_recvfrom(int s, int buf, int len, int flags, int from, int fromlen) {
            int bytesReceived = 0;
            try {
                bytesReceived = P.Rtrn_extern_recvfrom(s, buf, len, flags, from, fromlen);
            } catch { }
            if (bytesReceived > 0)
                data.StorePacket(0, buf, bytesReceived, from);
            return bytesReceived;
        }

        /// <summary>
        /// Print the packet capture buffer to a file.
        /// </summary>
        /// <param name="filename"></param>
        public static void Print(string filename) {
            if (data == null) {
                Core.WriteToChat($"Rolling PCap logger was not enabled. enable it with /ub pcap enable");
                return;
            }
            data.Print(filename);
        }

        internal static Data data;

        internal unsafe class Data {
            internal readonly int bufferDepth;
            internal int[] size;
            internal byte[,] data;
            internal long[] ts;
            internal int it = 0;
            internal int[] bytes = new int[2] { 0, 0 };
            internal int[] pkts = new int[2] { 0, 0 };
            public Data(int bufferDepth = 1000) {
                this.bufferDepth = bufferDepth;
                size = new int[bufferDepth];
                data = new byte[bufferDepth, 512];
                ts = new long[bufferDepth];
            }
            internal void StorePacket(int direction, int buf, int len, int otherEnd) {
                if (len > 511) {
                    short sin_family = *(short*)otherEnd;
                    ushort sin_port = *(ushort*)(otherEnd + 2);
                    sin_port = (ushort)((sin_port >> 8) | (sin_port << 8));
                    byte[] sin_address = new byte[] { *(byte*)(otherEnd + 4), *(byte*)(otherEnd + 5), *(byte*)(otherEnd + 6), *(byte*)(otherEnd + 7) };
                    byte[] arr = new byte[len];
                    Marshal.Copy((IntPtr)buf, arr, 0, len);
                    Core.WriteToChat($"PCAP Error: Oversized packet of {len} received: {(direction == 0 ? "From" : "To")} {sin_family}:{sin_address[0]}.{sin_address[1]}.{sin_address[2]}.{sin_address[3]}:{sin_port}\n{UBHelper.Core.HexDump(arr)}");
                    len = 511;
                }
                int thisit = Incit();
                pkts[direction]++;
                bytes[direction] += len;
                data[thisit, 511] = (byte)direction;
                size[thisit] = len;
                ts[thisit] = ((DateTime.UtcNow.ToFileTimeUtc() - 116444736000000000) / 10);
                for (int i = 0; i < len; i++)
                    data[thisit, i] = *(byte*)(buf + i);
            }
            internal int Incit() {
                it = (it + 1) % bufferDepth;
                return it;
            }
            internal void Print(string filename) {
                using (BinaryWriter writer = new BinaryWriter(File.Open(filename, FileMode.Create))) {
                    writer.Write((uint)0xA1B2C3D4);
                    writer.Write((uint)0x00040002);
                    writer.Write((uint)0x00000000);
                    writer.Write((uint)0x00000000);
                    writer.Write((uint)0x0000202A);
                    writer.Write((uint)0x00000001);

                    int thisit = Incit();
                    int ceil = thisit - 1;
                    int searched = 0;
                    while (size[thisit] == 0) {
                        searched++;
                        if (searched > bufferDepth) {
                            Core.WriteToChat("ERROR: Unable to find data!");
                            it = 0;
                            return;
                        }
                        thisit = Incit();
                    }
                    int floor = thisit;

                    int expSize = 0;
                    int expInBound = 0;
                    int expOutBound = 0;
                    uint start_ts_sec = (uint)(ts[thisit] / 1000000);
                    uint start_ts_usec = (uint)(ts[thisit] % 1000000);

                    // find first enc seq for send/recv packets
                    //double initialHandshake = 

                    //while (true)


                    while (true) {
                        //pcap header
                        writer.Write((uint)(ts[thisit] / 1000000)); //ts_sec
                        writer.Write((uint)(int)(ts[thisit] % 1000000)); //ts_usec
                        writer.Write((uint)(42 + size[thisit])); // incl_len
                        writer.Write((uint)(42 + size[thisit])); // orig_len
                                                                 //ether header
                        writer.Write((uint)0x01B22400); //first 2 octets of dmac
                        writer.Write((uint)0x00080302); //second 3rd octet of dmac, first octet of smac
                        writer.Write((uint)0x03020127); //last 2 octets of smac
                                                        // these should be swapped, but aclog keeps em static :shrug:
                        writer.Write((ushort)0x0008); // ether proto
                                                      // tcp header
                        writer.Write((uint)0x00000045);
                        writer.Write((uint)0x00000000);
                        writer.Write((uint)0x00001100);
                        if (data[thisit, 511] == 1) {
                            expOutBound++;
                            writer.Write((uint)0x0100007F);
                            writer.Write((uint)0xACD908CE);
                            writer.Write((uint)0x28233930);
                        } else {
                            expInBound++;
                            writer.Write((uint)0xACD908CE);
                            writer.Write((uint)0x0100007F);
                            writer.Write((uint)0x39302823);
                        }
                        //udp header
                        writer.Write(P.SwapBytes((short)(8 + size[thisit]))); //technically this should be ushort, but for the possible sizes, it shouldn't matter.
                        writer.Write((ushort)0x0000);
                        //AC Data
                        for (int i = 0; i < size[thisit]; i++)
                            writer.Write(data[thisit, i]);

                        expSize += size[thisit];

                        if (thisit == ceil) break;
                        thisit = Incit();
                    }
                    int end_ts_sec = (int)(ts[thisit] / 1000000);
                    Core.WriteToChat($"Wrote {expSize:n0} bytes of packet log data, consisting of {expInBound} inbound and {expOutBound} outbound packets.");
                    Core.WriteToChat($"   starting at {start_ts_sec}.{start_ts_usec} and lasting {end_ts_sec - start_ts_sec} seconds");
                    Core.WriteToChat($"   to filename {filename}");
                    Incit(); // put iterator back inplace
                }
            }
        }
    }

}