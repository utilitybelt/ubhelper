﻿using System;
using System.Runtime.InteropServices;

namespace UBHelper {
    /// <summary>
    /// Allegiance Manager Backend
    /// </summary>
    public static unsafe class Allegiance {

        /// <summary>
        /// Gets a pointer to the top AllegianceNode
        /// </summary>
        internal static int* MonarchPtr { get { try { return (int*)*(int*)(*P.ClientAllegianceSystem + 0x001C); } catch { return (int*)0; } } }

        /// <summary>
        /// Gets Allegiance Name
        /// </summary>
        public static string Name { get { try { return Core.PStringBase_char_read((int*)((*P.ClientAllegianceSystem) + 0x003C)); } catch { return ""; } } }

        /// <summary>
        /// Gets locked status of the Allegiance
        /// </summary>
        public static bool Locked { get { try { return (*(int*)(*P.ClientAllegianceSystem + 0x0118) == 1); } catch { return false; } } }


        /// <summary>
        /// Swear Allegiance to a target
        /// </summary>
        /// <param name="i_target">character id swear to</param>
        public static void SwearAllegiance(int i_target) {
            P.Call_CM_Allegiance__Event_SwearAllegiance(i_target);
        }
        /// <summary>
        /// Break Allegiance from a target
        /// </summary>
        /// <param name="i_target">character id break from</param>
        public static void BreakAllegiance(int i_target) {
            P.Call_CM_Allegiance__Event_BreakAllegiance(i_target);
        }



    }
}
