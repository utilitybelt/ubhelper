﻿using System;
using System.Threading;

namespace UBHelper {
    public static class SimpleFrameLimiter {
        private static bool _enabled = false;
        private static int _globalMax = 0;
        internal static int _globalTime = 0;
        public static int globalMax {
            get => _globalMax;
            set {
                if (value == _globalMax) return;
                _globalMax = value;
                _globalTime = value == 0 ? 0 : (1000 / value);
                if (value == 0 && _bgMax == 0) Enabled = false;
                else Enabled = true;
            }
        }
        private static int _bgMax = 0;
        internal static int _bgTime = 0;
        public static int bgMax {
            get => _bgMax;
            set {
                if (value == _bgMax) return;
                _bgMax = value;
                _bgTime = value == 0 ? 0 : (1000 / value);
                if (value == 0 && _globalMax == 0) Enabled = false;
                else Enabled = true;
            }
        }
        public static bool Enabled {
            get => _enabled;
            set {
                if (_enabled == value) return;
                _enabled = value;
                if (value) P.Device__DoFrameSleep.Setup(new P.def_Device__DoFrameSleep(Hook_Device__DoFrameSleep));
                else P.Device__DoFrameSleep.Remove();
            }
        }

        //note to future Yonneh- this logic was changed after 0.1.7 release
        private static void Hook_Device__DoFrameSleep() {
            if (!Core.isFocused && _bgMax != 0) Thread.Sleep(_bgTime);
            else Thread.Sleep(_globalTime);
        }
    }
}
