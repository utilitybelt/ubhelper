﻿using MetaViewWrappers.DecalControls;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace UBHelper {
    public class FellowMember {
        public int id;
        public string name;
        public int level;
        //public int share_loot;
        public int cur_health;
        //public int cur_stamina;
        //public int cur_mana;
        public int max_health;
        //public int max_stamina;
        //public int max_mana;
    }

    /// <summary>
    /// Helper methods for managing a ac fellowship
    /// </summary>
    public static unsafe class Fellow {
        internal static int FindFellow(int _to_find) {
            int fellow_member_ptr;
            try {
                fellow_member_ptr = *(int*)(((_to_find & (*(int*)(CFellowship + 0x10) - 1)) << 2) + *(int*)(CFellowship + 0x0C));
                while (fellow_member_ptr != 0) {
                    if (*(int*)(fellow_member_ptr) == _to_find) return fellow_member_ptr;
                    fellow_member_ptr = *(int*)(fellow_member_ptr + 0x34);
                }
            } catch { }
            return 0;
        }
        internal static int CFellowship { get { try { return (*(int*)(*P.ClientFellowshipSystem + 0x10)); } catch { return (int)0; } } }

        /// <summary>
        /// Returns true if the the player is in a fellowship
        /// </summary>
        public static bool InFellowship { get { try { return ((int)CFellowship != (int)0); } catch { return false; } } }

        /// <summary>
        /// Returns the name of the current fellowship
        /// </summary>
        public static string Name { get { try { return Core.PStringBase_char_read((int*)(CFellowship + 0x0018)); } catch { return ""; } } }

        /// <summary>
        /// get: Returns true if the current fellowship leader's character id
        /// set: Give the leader position of the current fellowship to character_id. This only works if you are the fellowship leader.
        /// </summary>
        /// <param name="value">the id of the character to give the leadership position to</param>
        public static int Leader {
            get { try { return (*(int*)(CFellowship + 0x1C)); } catch { return (int)0; } }
            set {
                if (!InFellowship) {
                    if (Core.Debug) Core.Error($"UBHelper.Fellow.Leader(0x{value:X8}): You are not in a fellowship!");
                    return;
                }
                if (!IsLeader) {
                    if (Core.Debug) Core.Error($"UBHelper.Fellow.Leader(0x{value:X8}): You are not the fellowship leader!");
                    return;
                }
                if (*P.CPhysicsPart__player_iid == value) {
                    if (Core.Debug) Core.Error($"UBHelper.Fellow.Leader(0x{value:X8}): You cannot set yourself as the leader!");
                    return;
                }
                if (FindFellow(value) == 0) {
                    if (Core.Debug) Core.Error($"UBHelper.Fellow.Leader(0x{value:X8}): Player is not in your fellowship!");
                    return;
                }
                P.Call_CM_Fellowship__Event_AssignNewLeader(value);
            }
        }

        /// <summary>
        /// Returns true if the player is the leader of the current fellowship
        /// </summary>
        public static bool IsLeader { get { try { return (Leader == *P.CPhysicsPart__player_iid); } catch { return false; } } }

        /// <summary>
        /// Returns the number of players in the current fellowship (very cheap)
        /// </summary>
        public static int MemberCount { get { try { return *(int*)(CFellowship + 0x14); } catch { return 0; } } }

        /// <summary>
        /// Returns a list of players in the current fellowship (very expensive)
        /// </summary>
        public static Dictionary<int,FellowMember> Members {
            get {
                Dictionary<int, FellowMember> res = new Dictionary<int, FellowMember>();

                if (!InFellowship) {
                    if (Core.Debug) Core.Error($"UBHelper.Fellow.Members: You are not in a fellowship!");
                    return res;
                }
                try {
                    int cur_fellow_ptr;
                    for (int i = 0; i < (*(int*)(CFellowship + 0x0010)); i++) {
                        cur_fellow_ptr = *(int*)(*(int*)(CFellowship + 0x0C) + (i << 2));
                        while (cur_fellow_ptr != 0) {
                            res.Add(*(int*)(cur_fellow_ptr), new FellowMember {
                                id = *(int*)(cur_fellow_ptr),
                                name = Core.PStringBase_char_read((int*)(cur_fellow_ptr + 0x08)),
                                level = *(int*)(cur_fellow_ptr + 0x0C),
                                //share_loot = *(int*)(cur_fellow_ptr + 0x18),
                                max_health = *(int*)(cur_fellow_ptr + 0x1C),
                                //max_stamina = *(int*)(cur_fellow_ptr + 0x20),
                                //max_mana = *(int*)(cur_fellow_ptr + 0x24),
                                cur_health = *(int*)(cur_fellow_ptr + 0x28),
                                //cur_stamina = *(int*)(cur_fellow_ptr + 0x2C),
                                //cur_mana = *(int*)(cur_fellow_ptr + 0x30)
                            });
                            cur_fellow_ptr = *(int*)(cur_fellow_ptr + 0x34);
                        }
                    }
                } catch { }
                return res;
            }
        }

        /// <summary>
        /// Returns true if the current fellowship is sharing xp
        /// </summary>
        public static bool ShareXP { get { try { return (*(int*)(CFellowship + 0x20) == 1); } catch { return false; } } }

        /// <summary>
        /// Returns true if the current fellowship is evenly splitting xp
        /// </summary>
        public static bool EvenXPSplit { get { try { return (*(int*)(CFellowship + 0x24) == 1); } catch { return false; } } }

        /// <summary>
        /// Gets or Sets the current fellowship Openess. Set only works if you are the fellowship leader.
        /// </summary>
        /// <param name="value">true/false</param>
        public static bool Open {
            get { try { return (*(int*)(CFellowship + 0x28) == 1); } catch { return false; } }
            set {

                if (!InFellowship) {
                    if (Core.Debug) Core.Error($"UBHelper.Fellow.Open({value}): You are not in a fellowship!");
                    return;
                }
                if (!IsLeader) {
                    if (Core.Debug) Core.Error($"UBHelper.Fellow.Open({value}): You are not the fellowship leader!");
                    return;
                }
                if (value) {
                    if (Open) {
                        if (Core.Debug) Core.Error($"UBHelper.Fellow.Open(true): Fellowship is already open!");
                        return;
                    }
                    P.Call_CM_Fellowship__Event_ChangeFellowOpeness(1);
                } else {
                    if (!Open) {
                        if (Core.Debug) Core.Error($"UBHelper.Fellow.Open(false): Fellowship is already closed!");
                        return;
                    }
                    P.Call_CM_Fellowship__Event_ChangeFellowOpeness(0);
                }
            }
        }

        /// <summary>
        /// Returns true if the current fellowship is Locked.
        /// </summary>
        public static bool Locked { get { try { return (*(int*)(CFellowship + 0x2C) == 1); } catch { return false; } } }

        /// <summary>
        /// Create an ac fellowship
        /// </summary>
        /// <param name="name">name of the fellowship to create</param>
        public static void Create(string name) {
            if (InFellowship) {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Create({name}): Already in a fellowship!");
                return;
            }
            if (name.Length == 0) {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Create({name}): Name cannot be blank!");
                return;
            }
            int* ps_message = Core.PStringBase_char_new(name);
            int share_xp = (Player.GetOption(Player.PlayerOption.FellowshipShareXP) ? 1 : 0);
            P.Call_CM_Fellowship__Event_Create((int)&ps_message, share_xp);
            Core.DisposeObject(ps_message);
        }
        /// <summary>
        /// Quit the current ac fellowship
        /// </summary>
        public static void Quit() {
            if (!InFellowship) {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Quit(): You are not in a fellowship!");
                return;
            }
            P.Call_CM_Fellowship__Event_Quit(0);
        }
        /// <summary>
        /// Returns true if the character is in your fellowship
        /// </summary>
        /// <param name="character_id">character id to find</param>
        public static bool IsInFellowship(int character_id) {
            if (!InFellowship) {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Quit(): You are not in a fellowship!");
                return false;
            }
            return FindFellow(character_id) != 0;
        }

        /// <summary>
        /// Disband the current ac fellowship
        /// </summary>
        public static void Disband() {
            if (!InFellowship) {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Disband(): You are not in a fellowship!");
                return;
            }
            if (!IsLeader) {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Disband(): You are not the fellowship leader!");
                return;
            }
            P.Call_CM_Fellowship__Event_Quit(1);
        }

        /// <summary>
        /// Dismiss a character from the fellowship.  Only works if you are the fellowship leader.
        /// </summary>
        /// <param name="character_id">character id to dismiss</param>
        public static void Dismiss(int character_id) {
            if (character_id == *P.CPhysicsPart__player_iid) { // workaround for Ace's janky behaviors.
                Quit();
                return;
            }
            if (!InFellowship) {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Dismiss(0x{character_id:X8}): You are not in a fellowship!");
                return;
            }
            if (!IsLeader) {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Dismiss(0x{character_id:X8}): You are not the fellowship leader!");
                return;
            }
            if (FindFellow(character_id) == 0) {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Dismiss(0x{character_id:X8}): Character not in fellowship!");
                return;
            }
            P.Call_CM_Fellowship__Event_Dismiss(character_id);
        }

        /// <summary>
        /// Recruit a character into the current ac fellowship. The fellowship must be open, or you must
        /// be the leader.
        /// </summary>
        /// <param name="character_id"></param>
        public static void Recruit(int character_id) {
            if (!InFellowship) {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Recruit(0x{character_id:X8}): You are not in a fellowship!");
                return;
            }
            if ((!IsLeader && !Open)) {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Recruit(0x{character_id:X8}): You are not the fellowship leader, and the fellowship is closed!");
                return;
            }
            if (FindFellow(character_id) != 0) {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Recruit(0x{character_id:X8}): Character already in fellowship!");
                return;
            }

            int physics = Core.GetPhysicsPtr(character_id);
            if (physics == 0) {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Recruit(0x{character_id:X8}): character not close enough! (error:2)");
                return;
            }
            float range = (float)999.99;
            try { range = *(float*)(physics + 0x20); } catch {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Recruit(0x{character_id:X8}): character not close enough! (error:3)");
                return;
            }
            if (range > 75) {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Recruit(0x{character_id:X8}): character is {range} meters away! (error:4)");
                return;
            }
            P.Call_CM_Fellowship__Event_Recruit(character_id);
        }

        /// <summary>
        /// Request a fellowship update from the server
        /// </summary>
        /// <param name="i_on">Set to true to get full data (player vitals)</param>
        public static void Update(bool i_on) {
            if (!InFellowship) {
                if (Core.Debug) Core.Error($"UBHelper.Fellow.Update({i_on}): You are not in a fellowship!");
                return;
            }
            P.Call_CM_Fellowship__Event_UpdateRequest((i_on ? 1 : 0));
        }
    }
}